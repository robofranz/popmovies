package org.blees.android.popmovies;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.blees.android.popmovies.utilities.DataFetcher;
import org.blees.android.popmovies.utilities.NetworkUtils;

import java.net.URL;

import static org.blees.android.popmovies.utilities.NetworkUtils.getResponseFromHttpUrl;

public class MainActivity extends AppCompatActivity
        implements RecyclerViewAdapter.ItemClickListener, RecyclerViewAdapter.EndOfDataListener {

    private NetworkUtils networkUtils;

    private MovieList movieList;
    private final MovieList movieListByPopularity = new MovieList();
    private final MovieList movieListByRating = new MovieList();

    private String tmdbApiKey;
    private ProgressBar progressBar;
    private RecyclerViewAdapter recyclerViewAdapter;

    private RecyclerView recyclerView;
    private Toast mToast;

    private DataFetcher dataFetcher;

    private JsonMovieListFetcherTask BackgroundMovieListFetcher = new JsonMovieListFetcherTask();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        networkUtils = new NetworkUtils(this);
        dataFetcher = new DataFetcher(this);
        tmdbApiKey = getString(R.string.tmdb_api_key);
        movieListByPopularity.setListName(getString(R.string.tmdb_keyword_popular));
        movieListByRating.setListName(getString(R.string.tmdb_keyword_rating));
        movieList = movieListByRating;
        progressBar = (ProgressBar) findViewById(R.id.main_activity_progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        GridLayoutManager layoutManager = new GridLayoutManager(MainActivity.this, 2);
        // Creating Adapter after the rest of the Layout is loaded.
        recyclerView = (RecyclerView) findViewById(R.id.pop_movies_recycler_view);
        recyclerView.setVisibility(View.INVISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager); // Set the layoutManager on recyclerView
        recyclerViewAdapter = new RecyclerViewAdapter(movieList, this, this, this);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    /**
     * Inflates the menu for the main activity.
     *
     * @param menu Menu to inflate.
     * @return True on success.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Called when a MenuItem has been selected. Determines which Item was selected.
     * Switches between the different MovieLists and notifies the RecyclerView Adapter
     * that his dataset has changed.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        recyclerView.smoothScrollToPosition(0);
        switch (item.getItemId()) {
            case R.id.menu_main_sort_pop:
                movieList = movieListByPopularity;
                recyclerViewAdapter.setData(movieList);
                recyclerViewAdapter.notifyDataSetChanged();
                return true;
            case R.id.menu_main_sort_rating:
                movieList = movieListByRating;
                recyclerViewAdapter.setData(movieList);
                recyclerViewAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Interface from the RecyclerView Adapter.
     * Gets called whenever a Item in the RecyclerView is clicked.
     * Loads the extra Details if the MovieData does not hold them.
     *
     * @param ClickedItemIndex Index of the clicked ViewHolder.
     */
    @Override
    public void onItemClicked(int ClickedItemIndex) {
        progressBar.setVisibility(View.VISIBLE);
        if (!movieList.movieHasDetails(ClickedItemIndex)) {
            String ClickedMovieId = movieList.getMovieId(ClickedItemIndex);
            new JsonMovieDetailFetcherTask().execute(tmdbApiKey, ClickedMovieId, String.valueOf(ClickedItemIndex));
        } else {
            startDetailActivity(ClickedItemIndex);
        }

    }

    /**
     * Interface from the RecyclerView Adapter.
     * Called when RecyclerView Adapter is near end of data.
     * Starts background task to load more data.
     */
    @Override
    public void onEndOfData(MovieList movieList) {
        if (BackgroundMovieListFetcher.getStatus() != AsyncTask.Status.RUNNING) {
            BackgroundMovieListFetcher = new JsonMovieListFetcherTask();
            int page = movieList.getCurrentPage();
            if (page < movieList.getTotalPages()) {
                progressBar.setVisibility(View.VISIBLE);
                Log.d("onEndOfData", "Reached End of Page #" + page);
                Log.d("onEndOfData", "Going to Load Page #" + (movieList.getCurrentPage()+1));
                BackgroundMovieListFetcher.execute(tmdbApiKey, movieList.getListName(),
                        Integer.toString(movieList.getCurrentPage()+ 1), movieList.getListName());
            }
        } else {
            Log.d("onEndOfData", "Not ready to Load data. Background Task Status: "
                    + BackgroundMovieListFetcher.getStatus());
        }


    }

    /**
     * Fetches movie list from TMDB in background.
     */
    private class JsonMovieListFetcherTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            String apiKey = params[0];
            String searchTerm = params[1];
            String pageNumber = params[2];
            String listName = params[3];
            String[] httpQueryRespond = new String[1];
            Log.d("JsonMovieListFetcher", "Searching for Page #" + pageNumber);
            URL TmdbRequestUrl = networkUtils.buildUrlForJson(apiKey, searchTerm, pageNumber);
            // Check if MainActivity did not change the list
            if (listName.equals(movieList.getListName())) {
                try {
                    httpQueryRespond[0] = NetworkUtils.getResponseFromHttpUrl(TmdbRequestUrl);
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
                if (httpQueryRespond[0] != null) {
                    Log.d("httpQueryRespond[0] : ", httpQueryRespond[0]);
                    // Update Items, current paga and total page count.
                    dataFetcher.fillMovieDataArray(httpQueryRespond[0], movieList);
                    movieList.setCurrentPage(movieList.getCurrentPage()+1);
                    movieList.setItemsPerPage(dataFetcher.getItemsPerPage(httpQueryRespond[0]));
                    movieList.setTotalPages(dataFetcher.getTotalPageCount(httpQueryRespond[0]));
                    return true;
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean params) {
            recyclerView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            if (params == false) {
                showToast("Connection Error");
            } else {
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        }
    }


    /**
     * Fetches movie details from tmdb in background.
     */
    private class JsonMovieDetailFetcherTask extends AsyncTask<String, Void, Integer> {
        @Override
        protected Integer doInBackground(String... params) {
            String apiKey = params[0];
            String searchTerm = params[1];
            String httpQueryRespond;
            int clickedItemIndex = Integer.valueOf(params[2]);
            URL TmdbRequestUrl = networkUtils.buildUrlForJson(apiKey, searchTerm);
            try {
                httpQueryRespond = getResponseFromHttpUrl(TmdbRequestUrl);
            } catch (java.io.IOException e) {
                e.printStackTrace();
                return null;
            }
            dataFetcher.fillMovieDetails(clickedItemIndex, httpQueryRespond, movieList);
            return clickedItemIndex;
        }

        @Override
        protected void onPostExecute(Integer ClickedItemId) {
            progressBar.setVisibility(View.INVISIBLE);
            if (ClickedItemId == null) {
                showToast("Connection Error");
            } else {
                startDetailActivity(ClickedItemId);
            }
        }
    }

    /**
     * Starts the Detail View of a movie.
     * Prepares the intent and starts the activity.
     *
     * @param ClickedItemId Which movie should be shown in detail.
     */
    private void startDetailActivity(int ClickedItemId) {
        String movieTitle = movieList.getMovieTitle(ClickedItemId);
        String movieYear = movieList.getMovieReleaseDate(ClickedItemId);
        String movieOverview = movieList.getMovieOverview(ClickedItemId);
        String moviePosterPath = movieList.getMoviePosterPath(ClickedItemId);
        String movieVoteAverage = movieList.getMovieVoteAverage(ClickedItemId);
        String movieVoteCount = movieList.getMovieVoteCount(ClickedItemId);
        String movieDuration = movieList.getMovieRuntime(ClickedItemId);

        Intent intent = new Intent(this, MovieDetailActivity.class);
        intent.putExtra(getString(R.string.movie_detail_intent_extra_message_title), movieTitle);
        intent.putExtra(getString(R.string.movie_detail_intent_extra_message_year), movieYear);
        intent.putExtra(getString(R.string.movie_detail_intent_extra_message_overview), movieOverview);
        intent.putExtra(getString(R.string.movie_detail_intent_extra_message_vote_average), movieVoteAverage);
        intent.putExtra(getString(R.string.movie_detail_intent_extra_message_vote_count), movieVoteCount);
        intent.putExtra(getString(R.string.movie_detail_intent_extra_message_poster_path), moviePosterPath);
        intent.putExtra(getString(R.string.movie_detail_intent_extra_message_duration), movieDuration);
        intent.putExtra(getString(R.string.movie_detail_intent_extra_message_poster_resolution), getString(R.string.tmdb_poster_resolution));
        startActivity(intent);
    }


    /**
     * Displays a text message as toast.
     * Takes care of canceling current toast, if a new one arrives.
     *
     * @param message Text to display.
     */
    private void showToast(String message) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        mToast.show();
    }
}
