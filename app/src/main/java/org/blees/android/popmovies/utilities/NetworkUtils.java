package org.blees.android.popmovies.utilities;

/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.blees.android.popmovies.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import static android.content.ContentValues.TAG;


/**
 * These utilities will be used to communicate with the weather servers.
 */
public final class NetworkUtils {
    private final Context context;
    private final String BASE_URL_JSON;
    private final String BASE_URL_POSTER;


    public NetworkUtils(Context context) {
        this.context = context;
        BASE_URL_JSON = this.context.getString(R.string.tmdb_json_base_path);
        BASE_URL_POSTER = this.context.getString(R.string.tmdb_poster_base_path);
    }

    /**
     * Build URL to load list from TMDB.
     *
     * @param apiKey     TMDB api key.
     * @param searchTerm Sort order.
     * @param pageNumber List page to load.
     * @return Query URL.
     */
    public URL buildUrlForJson(String apiKey, String searchTerm, String pageNumber) {
        Uri builtUri = Uri.parse(BASE_URL_JSON).buildUpon()
                .appendEncodedPath(searchTerm)
                .appendQueryParameter(context.getString(R.string.tmdb_keyword_api_key), apiKey)
                .appendQueryParameter(context.getString(R.string.tmdb_keyword_page), pageNumber)
                .build();
        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * Build URL to load movie details from TMDB.
     *
     * @param apiKey     TMDB api key.
     * @param searchTerm Sort order.
     * @return Query URL.
     */
    public URL buildUrlForJson(String apiKey, String searchTerm) {
        Uri builtUri = Uri.parse(BASE_URL_JSON).buildUpon()
                .appendEncodedPath(searchTerm)
                .appendQueryParameter(context.getString(R.string.tmdb_keyword_api_key), apiKey)
                .build();
        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * Build URL to load movie details from TMDB.
     *
     * @param posterSize Size of poster.
     * @param posterPath Path to append to URL.
     * @return URL of movie poster.
     */
    public URL buildUrlForPoster(String posterSize, String posterPath) {
        Uri builtUri = Uri.parse(BASE_URL_POSTER).buildUpon()
                .appendEncodedPath(posterSize)
                .appendEncodedPath(posterPath)
                .build();
        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * Return result from HTTP Query as String.
     *
     * @param url URL to query.
     * @return HTTP response.
     * @throws IOException IOException for network and stream reading.
     */
    /*
    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();
            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");
            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
    */
    public static String getResponseFromHttpUrl(URL url) throws IOException {
        String response = null;
        InputStream inputStream = null;
        HttpURLConnection urlConnection = null;
        try {
                /* forming th java.net.URL object */
            urlConnection = (HttpURLConnection) url.openConnection();

                 /* optional request header */
            urlConnection.setRequestProperty("Content-Type", "application/json");

                /* optional request header */
            urlConnection.setRequestProperty("Accept", "application/json");

                /* for Get request */
            urlConnection.setRequestMethod("GET");
            int statusCode = urlConnection.getResponseCode();

                /* 200 represents HTTP OK */
            if (statusCode == 200) {
                inputStream = new BufferedInputStream(urlConnection.getInputStream());
                response = convertInputStreamToString(inputStream);
            }
        } catch (Exception e) {
            Log.d(TAG, e.getLocalizedMessage());
        }
        return response;
    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null){
            result += line;
        }

            /* Close Stream */
        if(null!=inputStream){
            inputStream.close();
        }
        return result;
    }


}

