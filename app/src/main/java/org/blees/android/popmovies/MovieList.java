package org.blees.android.popmovies;

import android.util.Log;

import java.util.ArrayList;

/**
 * Represents a List of Movies
 */
public class MovieList {
    private int length;
    private int itemsPerPage;
    private int currentPage = 0;
    private int totalPages = 1;
    private String listName;
    private ArrayList<MovieData> movieDataArrayList;

    public MovieList() {
        movieDataArrayList = new ArrayList<>();
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public int getLength() {
        return length;
    }

    public ArrayList getMovieDataArrayList() {
        return movieDataArrayList;
    }

    public void setMovieDataArrayList(ArrayList<MovieData> movieDataArrayList) {
        this.movieDataArrayList = movieDataArrayList;
    }

    public MovieData getMovieData(int index) {
        return movieDataArrayList.get(index);
    }

    public void addMovie(String movieId, String movieTitle, String moviePosterPath,
                         String movieOverview, String movieReleaseDate, String movieVoteAverage,
                         String movieVoteCount) {

       MovieData movieData = new MovieData(movieId, movieTitle, moviePosterPath, movieOverview,
               movieReleaseDate, movieVoteAverage, movieVoteCount);
        Log.d("addMovie", "Created the movie at Position : " + movieDataArrayList.size());
       movieDataArrayList.add(movieData);
       length++;
    }

    public void setMovieRuntime(int index, String runtime) {
        movieDataArrayList.get(index).setRuntime(runtime);
    }

    public boolean movieHasDetails(int index) {
        return movieDataArrayList.get(index).hasDetails();
    }

    public String getMovieId(int index) {
        return movieDataArrayList.get(index).getId();
    }

    public String getMovieTitle(int index) {
        return movieDataArrayList.get(index).getTitle();
    }

    public String getMovieReleaseDate(int index) {
        return movieDataArrayList.get(index).getReleaseDate();
    }

    public String getMovieVoteAverage(int index) {
        return movieDataArrayList.get(index).getVoteAverage();
    }

    public String getMovieVoteCount(int index) {
        return movieDataArrayList.get(index).getVoteCount();
    }

    public String getMovieOverview(int index) {
        return movieDataArrayList.get(index).getOverview();
    }

    public String getMovieRuntime(int index) {
        return movieDataArrayList.get(index).getRuntime();
    }

    public String getMoviePosterPath(int index) {
        return movieDataArrayList.get(index).getPosterPath();
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * Find Id of movie in the MovieData Array.
     *
     * @param searchId TMDB movie id.
     * @return Index in the array.
     */
    public int findMovieIndexById(String searchId) {
        int movieIndex = 0;
        for (MovieData element : movieDataArrayList) {
            if (element.getId().equals(searchId)) {
                return movieIndex;
            }
            movieIndex++;
        }
        return -1;
    }

    /**
     * Represents a single Movie.
     */
    private class MovieData {

        private String id, title, posterPath, overview, releaseDate, voteAverage, voteCount, runtime;

        /**
         * Movie Data gets stored in a set of Strings.
         *
         * @param movieId          TMDB ID of the movie.
         * @param movieTitle       Title of the movie.
         * @param moviePosterPath  Path to append for API Call.
         * @param movieOverview    Text presenting a overview of the movie.
         * @param movieReleaseDate Date the movie was released.
         * @param movieVoteAverage Average user rating.
         */
        public MovieData(String movieId, String movieTitle, String moviePosterPath, String movieOverview, String movieReleaseDate, String movieVoteAverage, String movieVoteCount) {
            Log.d("MovieData", "Created the movie");
            id = movieId;
            title = movieTitle;
            posterPath = moviePosterPath;
            overview = movieOverview;
            releaseDate = movieReleaseDate;
            voteAverage = movieVoteAverage;
            voteCount = movieVoteCount;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setOriginalTitle(String movieOriginalTitle) {
            title = movieOriginalTitle;
        }

        public String getPosterPath() {
            return posterPath;
        }

        public void setPosterPath(String moviePosterPath) {
            posterPath = moviePosterPath;
        }

        public String getOverview() {
            return overview;
        }

        public void setOverview(String movieOverview) {
            overview = movieOverview;
        }

        public String getReleaseDate() {
            return releaseDate;
        }

        public void setReleaseDate(String movieReleaseDate) {
            releaseDate = movieReleaseDate;
        }

        public String getVoteAverage() {
            return voteAverage;
        }

        public void setVoteAverage(String movieVoteAverage) {
            voteAverage = movieVoteAverage;
        }

        public String getVoteCount() {
            return voteCount;
        }

        public void setVoteCount(String voteCount) {
            this.voteCount = voteCount;
        }


        public String getRuntime() {
            return runtime;
        }

        public void setRuntime(String movieRuntime) {
            runtime = movieRuntime;
        }

        /**
         * sets the movie details which are not in the popular and top_rated api-call results.
         */

        public void setDetails(String movieRuntime) {
            runtime = movieRuntime;
        }


        /**
         * hasDetails returns true if all the data for the movie detail view has been fetched.
         */
        public boolean hasDetails() {
            return runtime != null;
        }

    }
}