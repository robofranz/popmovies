package org.blees.android.popmovies;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.blees.android.popmovies.utilities.NetworkUtils;

/**
 * Movie Details Activity.
 */
public class MovieDetailActivity extends AppCompatActivity {

    /**
     * Looks up the intent which started it and loads data to views.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        NetworkUtils networkUtils = new NetworkUtils(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Get the Intent that started this activity and extract the string.
        Intent intent = getIntent();

        // Get the data.
        String movieTitle = intent.getStringExtra(getString(R.string.movie_detail_intent_extra_message_title));
        String movieYear = intent.getStringExtra(getString(R.string.movie_detail_intent_extra_message_year));
        String movieVoteAverage = intent.getStringExtra(getString(R.string.movie_detail_intent_extra_message_vote_average));
        String movieVoteCount = intent.getStringExtra(getString(R.string.movie_detail_intent_extra_message_vote_count));
        String movieOverview = intent.getStringExtra(getString(R.string.movie_detail_intent_extra_message_overview));
        String moviePosterPath = intent.getStringExtra(getString(R.string.movie_detail_intent_extra_message_poster_path));
        String moviePosterResolution = intent.getStringExtra(getString(R.string.movie_detail_intent_extra_message_poster_resolution));
        String movieDuration = intent.getStringExtra(getString(R.string.movie_detail_intent_extra_message_duration));

        // Url string of picture.
        String imageUrl = networkUtils.buildUrlForPoster(moviePosterResolution, moviePosterPath).toString();

        // Capture the layout's TextViews.
        TextView movieTitleTv = (TextView) findViewById(R.id.movie_detail_title);
        TextView movieYearTv = (TextView) findViewById(R.id.movie_detail_release_date);
        TextView movieVoteAverageTv = (TextView) findViewById(R.id.movie_detail_rating);
        TextView movieOverviewTv = (TextView) findViewById(R.id.movie_detail_synopsis);
        TextView movieDurationTv = (TextView) findViewById(R.id.movie_detail_duration);
        ImageView moviePosterTv = (ImageView) findViewById(R.id.movie_detail_thumbnail);

        // Check the data.
        if (movieDuration.equals("0") || movieDuration.isEmpty()) {
            movieDurationTv.setVisibility(ImageView.INVISIBLE);
        }
        if (movieVoteCount.equals("0") || movieDuration.isEmpty()) {
            movieVoteAverageTv.setVisibility(ImageView.INVISIBLE);
        }

        // Fill the views.
        movieTitleTv.setText(movieTitle);
        movieYearTv.setText(movieYear);
        movieVoteAverageTv.setText(getString(R.string.movie_detail_average_rating, movieVoteAverage));
        movieDurationTv.setText(getString(R.string.movie_detail_duration, movieDuration));
        movieOverviewTv.setText(movieOverview);
        Context ImageContext = moviePosterTv.getContext();
        Picasso.with(ImageContext).load(imageUrl).into(moviePosterTv);

    }

    /**
     * Check which MenuItem was selected.
     *
     * @param item Id of the MenuItem.
     * @return true if we handled the selection.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
