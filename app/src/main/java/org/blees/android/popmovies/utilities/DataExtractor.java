package org.blees.android.popmovies.utilities;

import android.content.Context;

import org.blees.android.popmovies.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataExtractor {
    private final JSONArray jsonArray;
    private final int itemCount;
    private final Context context;

    /**
     * Read data from JSONArray.
     *
     * @param context coontext of activity
     * @param jsonArray JSONArray to read.
     */
    public DataExtractor(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
        itemCount = jsonArray.length();
    }

    /**
     * Read movie data from JSONObject.
     *
     * @param listItem List item to extract.
     * @return String[] holding movie data.
     */
    public String[] getMovie(int listItem) {
        String[] returnString = new String[7];
        for(int i = 0; i < returnString.length; i++){
            returnString[i] = "";
        }
        if (listItem <= itemCount) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(listItem);
                // Storing each json item in variable
                if (jsonObject.getString(context.getString(R.string.tmdb_keyword_id)) != null){
                    returnString[0] = jsonObject
                            .getString(context.getString(R.string.tmdb_keyword_id));
                }
                if(jsonObject.getString(context.getString(R.string.tmdb_keyword_title)) != null){
                    returnString[1] = jsonObject
                            .getString(context.getString(R.string.tmdb_keyword_title));
                }
                if (jsonObject.getString(context.getString(R.string.tmdb_keyword_poster_path)) != null){
                    returnString[2] = jsonObject
                            .getString(context.getString(R.string.tmdb_keyword_poster_path));
                }
                if (jsonObject.getString(context.getString(R.string.tmdb_keyword_overview)) != null){
                    returnString[3] = jsonObject
                            .getString(context.getString(R.string.tmdb_keyword_overview));
                }
                if (jsonObject.getString(context.getString(R.string.tmdb_keyword_release_date)) != null){
                    returnString[4] = jsonObject
                            .getString(context.getString(R.string.tmdb_keyword_release_date));
                }
                if (jsonObject.getString(context.getString(R.string.tmdb_keyword_vote_average)) != null){
                    returnString[5] = jsonObject
                            .getString(context.getString(R.string.tmdb_keyword_vote_average));
                }
                if (jsonObject.getString(context.getString(R.string.tmdb_keyword_vote_count)) != null) {
                    returnString[6] = jsonObject
                            .getString(context.getString(R.string.tmdb_keyword_vote_count));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return returnString;
    }

    public int getItemCount() {
        return itemCount;
    }
}