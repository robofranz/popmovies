package org.blees.android.popmovies.utilities;


import android.content.Context;
import android.util.Log;

import org.blees.android.popmovies.MovieList;
import org.blees.android.popmovies.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOError;

public class DataFetcher {
    private final Context context;

    public DataFetcher(Context context) {
        this.context = context;
    }

    /**
     * Fill return of api-call in MovieData Array.
     *
     * @param listString String holding the HTTP Respond.
     * @return True on success.
     */
    public boolean fillMovieDataArray(String listString, MovieList movieList) {
        JSONArray jsonArray = null;
        if (listString != null) {
            JSONObject jsonObject = getJson(listString);
            Log.d("fillMovieDataArray", "jsonResponse");
            try {
                jsonArray = jsonObject.getJSONArray("results");
            } catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }
            if (jsonArray != null) {
                DataExtractor DataExtractor = new DataExtractor(context, jsonArray);
                for (int i = 0; i < DataExtractor.getItemCount(); i++) {
                    String movieData[] = DataExtractor.getMovie(i);
                    // Split Date to Array
                    String date[] = movieData[4].split("-");
                    // Add new Movie to List
                    movieList.addMovie(movieData[0], movieData[1], movieData[2], movieData[3], date[0], movieData[5], movieData[6]);
                    Log.d("fillMovieDataArray", "Added MovieData to List on Position # " + movieList.getLength() + " - " + movieData[1]);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Creates JSON Object from HTTP Respond.
     *
     * @param textString HTTP respond.
     * @return JSONObject of respond.
     */
    private JSONObject getJson(String textString) {
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(textString);
            return jsonObject;
        } catch (JSONException e) {
            return null;
        }

    }

    /**
     * Reads list meta data from JSONObject.
     *
     * @param jsonObject Holds tmdb page as JSONObject.
     * @return Array of list data.
     */
    private String[] getListData(JSONObject jsonObject) {
        String[] metaData = new String[2];
        try {
            metaData[0] = jsonObject.getString(context.getString(R.string.tmdb_keyword_total_results));
            metaData[1] = jsonObject.getString(context.getString(R.string.tmdb_keyword_total_pages));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return metaData;
    }

    /**
     * Called when a Movie Detail View is created.
     * Fills movie details to MovieList.
     *
     * @param movieId        TMDB ID of movie.
     * @param jsonTextString String holding JSON Data movie.
     * @return Return True on success.
     */
    public boolean fillMovieDetails(int movieIndex, String jsonTextString, MovieList movieList) {
        if (jsonTextString != null) {
            if (movieIndex >= 0) {
                JSONObject jsonResponse = getJson(jsonTextString);
                String movieRuntime = getMovieRuntime(jsonResponse);
                // Check if respond is null, set runtime.
                if (movieRuntime != null || movieRuntime != "null" ) {
                    movieList.setMovieRuntime(movieIndex,movieRuntime);
                    return true;
                } else {
                    movieList.setMovieRuntime(movieIndex, "0");
                }
            }
        }
        return false;
    }

    /**
     * Reads movie details from JSONObject.
     *
     * @param jsonObject Holds JSONObject of movie detail page.
     * @return String Array of details from movie.
     */
    private String getMovieRuntime(JSONObject jsonObject) {
        String movieDetails;
        try {
            movieDetails = jsonObject.getString(context.getString(R.string.tmdb_keyword_runtime));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return movieDetails;
    }

    /**
     * Fetches Total Number of Pages available.
     *
     * @param httpQueryRespond http respond.
     * @return Total page count.
     */
    public int getTotalPageCount(String httpQueryRespond) {
        JSONObject jsonObject = getJson(httpQueryRespond);
        try {
            String[] metaData = getListData(jsonObject);
            Log.d("getTotalPages", metaData[1]);
            return Integer.parseInt(metaData[1]);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Look up items per page.
     *
     * @param jsonTextString HTTP respond of list.
     * @return Item count on page.
     */
    public int getItemsPerPage(String jsonTextString) {
        int itemsPerPage = 0;
        JSONArray mJsonMovieArray = null;
        if (jsonTextString != null) {
            JSONObject jsonResponse = getJson(jsonTextString);
            try {
                mJsonMovieArray = jsonResponse.getJSONArray(context.getString(R.string.tmdb_keyword_results));
            } catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }
            if (mJsonMovieArray != null) {
                DataExtractor DataExtractor = new DataExtractor(context, mJsonMovieArray);
                itemsPerPage = DataExtractor.getItemCount();
            }
        }
        return itemsPerPage;
    }

}

