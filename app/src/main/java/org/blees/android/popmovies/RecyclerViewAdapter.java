package org.blees.android.popmovies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Callback;
import org.blees.android.popmovies.utilities.NetworkUtils;

/**
 * Implementation of the RecyclerView
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private final Context context;
    private final NetworkUtils networkUtils;
    private final ItemClickListener itemClickListener;
    private final EndOfDataListener endOfDataListener;
    private MovieList movieList;

    /**
     * Constructor for RecyclerView
     *
     * @param movieList Movie Data in ArrayList.
     * @param itemClickListener Instance of implememtation, to receive clicked elements.
     * @param endOfDataListener Instance of implememtation, called on end of data.
     * @param context Context to run in.
     */
    public RecyclerViewAdapter(MovieList movieList, ItemClickListener itemClickListener, Context context, EndOfDataListener endOfDataListener) {
        this.context = context;
        this.itemClickListener = itemClickListener;
        this.movieList = movieList;
        this.endOfDataListener = endOfDataListener;
        networkUtils = new NetworkUtils(context);
        // If list is empty, fetch data.
        if (this.movieList.getLength() == 0) {
            this.endOfDataListener.onEndOfData(movieList);
        }
    }


    /**
     * ViewHolder implementation with onClickListener.
     * Provides a reference to each view representing a data item.
     * onClick sets the
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        public final FrameLayout viewHolder;

        public ViewHolder(FrameLayout layout) {
            super(layout);
            layout.setOnClickListener(this);
            this.viewHolder = layout;
        }

        /**
         * Called when a the ViewHolder is clicked
         * Passes the clicked position index back to the MainActivity
         */
        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            itemClickListener.onItemClicked(clickedPosition);
        }
    }

    /**
     * Create ViewHolder based on a specific layout.
     * Called from the layout manager.
     *
     * @param parent To get Context.
     * @param viewType View Typ.
     * @return viewHolder to be used by 'onBindViewHolder'.
     */
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.viewholder, parent, false);
        ViewHolder viewHolder = new ViewHolder(frameLayout);
        return viewHolder;
    }

    /**
     * Called to populate a ViewHolder with data.
     *
     * @param holder   ViewHolder to fill.
     * @param position Element of the dataset.
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String posterPath = movieList.getMoviePosterPath(position);
        ImageView moviePoster = holder.viewHolder.findViewById(R.id.viewholder_item_image);
        final TextView movieTitle = holder.viewHolder.findViewById(R.id.viewholder_item_title);
        Context imageContext = moviePoster.getContext();
        String imageUrl = networkUtils.buildUrlForPoster(context.getString(R.string.tmdb_poster_resolution), posterPath).toString();
        movieTitle.setVisibility(View.INVISIBLE);
        Picasso.with(imageContext)
                .load(imageUrl)
                .placeholder(R.drawable.movie_poster_placeholder)
                .into(moviePoster, new Callback() {
                    @Override public void onSuccess() {
                    }

                    @Override public void onError() {
                        movieTitle.setVisibility(View.VISIBLE);
                        movieTitle.setText(movieList.getMovieTitle(position));
                    }
                });

        int arrayLength = movieList.getLength();
        Log.d("onBindViewHolder", "ArrayLength = " + arrayLength);
        Log.d("onBindViewHolder", "AdapterPosition = " + position);
        if (position >= arrayLength -1 ) {
            endOfDataListener.onEndOfData(movieList);
        }

    }

    /**
     * Return the size of the dataset.
     *
     * @return Count of Elements to Display.
     */
    @Override
    public int getItemCount() {
        return movieList.getLength();
    }


    /**
     * Changes the dataset.
     *
     * @param movieList MovieList to display.
     */
    public void setData(MovieList movieList) {
        this.movieList = movieList;
        // If list is empty, fetch data.
        if (this.movieList.getLength() == 0) {
            endOfDataListener.onEndOfData(movieList);
        }
    }

    /**
     * Called when a item is clicked.
     */
    public interface ItemClickListener {
        void onItemClicked(int ClickedItemId);
    }

    /**
     * Called when reached end of data.
     */
    public interface EndOfDataListener {
        void onEndOfData(MovieList movieList);
    }
}
